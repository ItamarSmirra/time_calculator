import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const constTimes = [
    {
        day: {
            start: "09:05",
            end: "19:45",
            name: "שעות פעילות",
            serialNumber: 1
        },
        lunch: {
            start: "11:30",
            end: "13:05",
            name: "ארוחת צהריים",
            serialNumber: 2
        },
        minha: {
            start: "14:35",
            end: "14:55",
            name: "מנחה",
            serialNumber: 3
        },
        dinner: {
            start: "18:10",
            end: "18:50",
            name: "ארוחת ערב",
            serialNumber: 4
        },
    },
    {
        day: {
            start: "08:00",
            end: "21:30",
            name: "שעות פעילות",
            serialNumber: 1
        },
        lunch: {
            start: "11:40",
            end: "12:40",
            name: "ארוחת צהריים",
            serialNumber: 2
        },
        dinner: {
            start: "17:15",
            end: "18:20",
            name: "ארוחת ערב",
            serialNumber: 3
        },
    },
    {
        day: {
            start: "08:00",
            end: "21:30",
            name: "שעות פעילות",
            serialNumber: 1
        },
        lunch: {
            start: "11:35",
            end: "12:35",
            name: "ארוחת צהריים",
            serialNumber: 2
        },
        dinner: {
            start: "17:15",
            end: "18:20",
            name: "ארוחת ערב",
            serialNumber: 3
        },
    }
]

const store = {
  
    state: {
        courses: [
            {
                name: 'חרדים',
                code: 1
            },
            {
                name: "כל''צ",
                code: 2
            },
            {
                name: "טכ''מ",
                code: 3
            },
        ],
        currCourse: 1,
        times: []
    },
    mutations: {
        changeCourseTimes(state, payload) {
            state.times = payload;
        },
        changeCourse(state, payload) {
            state.currCourse = payload;         
        }
    },
    actions: {
        changeCourse({commit, state}, courseCode) {
            commit('changeCourse', courseCode);
            localStorage.setItem("currCourse", courseCode);
            commit('changeCourseTimes', JSON.parse(localStorage.getItem(state.currCourse + "times")));
        },
        saveBreak({commit, state}, newBreak) {
            const newTime = state.times;
            for (let key in newTime) {
                if (newTime[key].name == newBreak.name) {
                    newTime[key] = newBreak;
                    break;
                }
            }

            commit('changeCourseTimes', newTime);
            localStorage.setItem(state.currCourse + "times", JSON.stringify(newTime));      
        },
        firstMount({commit, state}) {
            if (!localStorage.getItem("moveToNewTimes")) {
                localStorage.clear();
                localStorage.setItem("moveToNewTimes", "yay");
            }

            if (localStorage.getItem("currCourse") == null) {
                localStorage.setItem("currCourse", 1);
            }

            commit('changeCourse', parseInt(localStorage.getItem("currCourse")));

            state.courses.map((course) => {
                if (localStorage.getItem(course.code + "times") == null) {
                    localStorage.setItem(course.code + "times", JSON.stringify(constTimes[course.code - 1]));
                }
            });
            
            commit('changeCourseTimes', JSON.parse(localStorage.getItem(state.currCourse + "times")));
        },
        resetTimes({commit, state}) {
            localStorage.setItem(state.currCourse + "times", JSON.stringify(constTimes[state.currCourse - 1]));
            commit('changeCourseTimes', JSON.parse(localStorage.getItem(state.currCourse + "times")));
        }
    }
};

export default new Vuex.Store(store);